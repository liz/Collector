# The Collector

PluralKit Wallpaper Switcher Slideshow

## Background

I wanted a way for my system switches in PluralKit to automatically change my devices' wallpapers. This is the solution I came up with for my Windows desktop.

The Collector is a web-based Wallpaper Engine wallpaper that listens to a socket connection for any fronter switch. Unfortunately, PluralKit doesn't provide an easy way to hook into this process. I had to create my own API that sends the updated member ID over a socket connection as well as call the standard PluralKit API. ⚠️ This portion of the project has not been released yet. You will need to make your own socket server for triggering the wallpaper changes for this software. You can see what my personal NodeRed setup looks like for this in [node_red.png](https://git.corrupt.link/liz/Collector/raw/branch/main/node_red.png)

## Expected socket response

```text
<member id>
```

## Wallpaper selection process

Once you select a directory in the Wallpaper Engine config UI you can drop any images there you'd like. Just make sure to include the member ID in the pathname. (Either in the filename of the image itself or in a folder with the ID in the name) The Collector can handle multiple member IDs for a single file.

## Limitation

This wallpaper can handle up to 10,240 images because of the Windows max path length and `locaStorage` limits.

## Apache 2.0 License

Copyright 2023 Liz Cray

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
